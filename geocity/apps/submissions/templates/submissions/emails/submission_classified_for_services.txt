{% load i18n %}{% autoescape off %}{% blocktrans %}Bonjour{% endblocktrans %},

{% trans "Nous vous informons qu'une demande a été traitée et classée par le secrétariat." %}

{% trans "Vous pouvez la consulter sur le lien suivant" %}: {{ submission_url }}

{% trans "Avec nos meilleures salutations," %}
{% if administrative_entity.custom_signature %}
{{ administrative_entity.custom_signature }}
{% else %}
{{ administrative_entity.name }}
{% endif %}

{% trans "Ceci est un e-mail automatique, veuillez ne pas y répondre." %}
{% endautoescape %}
