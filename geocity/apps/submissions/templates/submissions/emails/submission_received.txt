{% load i18n %}{% autoescape off %}{% blocktrans %}Bonjour {{ name }}{% endblocktrans %},

{% trans "Nous vous informons que votre demande/annonce a été prise en compte et classée." %}

{% trans "Vous pouvez la consulter sur le lien suivant" %}: {{ submission_url }}

{% trans "Avec nos meilleures salutations," %}
{% if administrative_entity.custom_signature %}
{{ administrative_entity.custom_signature }}
{% else %}
{{ administrative_entity.name }}
{% endif %}

{% trans "Ceci est un e-mail automatique, veuillez ne pas y répondre." %}
{% endautoescape %}
